+++
date = "2023-10-16T14:00:00+02:00"
title = "cocarto : billet d’étape n°10 (octobre 2023)"
author = "Nicolas"
categories = "cocarto"
+++

[cocarto](https://cocarto.com/) est un outil de saisie de données cartographiques et territoriales. Vous lisez la dixième édition de la [lettre mensuelle](https://buttondown.email/cocarto) qui en raconte les développements, doutes, détails inutiles…

## Petites entreprises

Le mois dernier, nous étions présents aux Géodatadays à Reims pour montrer cocarto sur grand écran:

![Tristram, en train de présenter cocarto sur la grande scène du centre des congrès de Reims pendant les geodatadays](IMG_9424.jpeg)

On y a croisé pas mal de monde, et appris plus qu’il ne serait possible de résumer ici. En tout cas, ça confirme qu’un outil comme cocarto a sa place dans cet écosystème, même si nombre d’acteurs ont plus l’habitude d’entendre parler d’[ESRI](https://en.wikipedia.org/wiki/Esri) que de coopératives. Vous pouvez retrouver la présentation et les notes [ici](GDD-2023-cocarto.pptx).

## Chaussures pointues

Depuis deux ans qu’on a commencé à écrire les premiers embryons de code de ce qui allait devenir cocarto, on se répète « aller, on finit *cette* fonctionnalité et ça sera la version 1.0 ».

Ça commence à être vrai: dans la grande barre de progression du *minimum viable product*, on en est à *la page pricing*: c’est  en discussion [ici](https://gitlab.com/CodeursEnLiberte/cocarto/-/merge_requests/635). Pour résumer, cocarto sera disponible gratuitement avec quelques limitations, sur abonnement pour certaines fonctionnalités, mais c’est surtout en faisant du conseil et des développements spécifiques que nous comptons ~~devenir millionaires~~ financer le développement. Quoi qu’il arrive, le projet restera libre et ouvert.

## Des carrés dans des ronds

[La dernière fois](http://www.codeursenliberte.fr/blog/cocarto_aout/), nous avons mentionné la configuration de l’import de données. C’est désormais fonctionnel et partiellement automatisé: par exemple, si les colonnes de votre fichier csv s’appellent `id_commune` et `population_2022`, la correspondance avec les champs “ID commune” et “Population (2022)” sera automatique dans cocarto. Ça marche aussi SI LES COLONNES LAT ET LONG SONT EN MAJUSCULES. Ou l’inverse.

![Les options de configuration d’import dans cocarto: la correspondance automatique des colonnes](auto-mapping.png)

## Des vecteurs dans des tuiles

Une autre avancée majeure depuis la dernière fois est plus technique. Désormais, les données de la carte sont directement servies sous forme de [tuiles MVT](https://postgis.net/docs/ST_AsMVT.html). Auparavant, on utilisait un geojson commun entre les données tabulaires et la carte. Ce changement nous permet d’améliorer les performances et d’appréhender la gestion de filtres et de tri de façon beaucoup plus sereine.

Pour les curieux, les détails [sont ici](https://gitlab.com/CodeursEnLiberte/cocarto/-/merge_requests/524).

## Dans le bateau

Jusque là, cocarto était, pour résumer, une équipe de _deux ETP_ : [Thibaut](http://www.codeursenliberte.fr/a-propos/qui/#thibaut) au design quelques jours par mois, [Tristram](http://www.codeursenliberte.fr/a-propos/qui/#tristram) et [Nicolas](http://www.codeursenliberte.fr/a-propos/qui/#nicolas) dans le code quand ils ne sont pas occupés ailleurs. Nous avons été rejoints ces dernières semaines par [Enkhé](http://www.codeursenliberte.fr/a-propos/qui/#enkhe), en stage depuis les [descodeuses](https://descodeuses.org), et par [Pierre](http://www.codeursenliberte.fr/a-propos/qui/#pierre), qui répondait déjà à notre confusion javascript pendant la sieste et après le bib.

---

C’est tout pour cette fois-ci! Merci de nous avoir lus; normalement, d’ici la prochaine fois, nous aurons amélioré [la gestion des pièces jointes](https://gitlab.com/CodeursEnLiberte/cocarto/-/merge_requests/620), ainsi que [reformulé les emails](https://gitlab.com/CodeursEnLiberte/cocarto/-/issues/316) envoyés par cocarto. N’hésitez pas à nous envoyer vos commentaires sur [bonjour@cocarto.com](mailto:bonjour@cocarto.com)

