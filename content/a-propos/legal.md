+++
date = "2016-11-05T20:01:41+02:00"
title = "Mentions légales"
no_date = true
+++

## Codeurs en Liberté

* SAS coopérative au capital variable de 8200 €
* 118 avenue Jean Jaurès, 75019 Paris
* **SIRET**  821611431 00031
* **Responsable de publication** Vincent Lara
* **Contact** `bonjour@codeursenliberté.fr`

## Notre hébergeur

* OVH
* 2 rue Kellermann - 59100 Roubaix

## 🍪 Cookies et vie privée

Nous ne plaçons aucun cookie sur votre navigateur et nous
ne transmettons à des tiers aucune information vous concernant.

Nous conservons le journal d’accès au serveur http, à savoir :

* Horodotage ;
* Adresse IP ;
* Page accédée ;
* Le _User-Agent_ déclaré par votre navigateur.

## Licence et charte d’éthique

L’ensemble du contenue du site est réutilisable en respectant les conditions
de [la licence CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/fr/).

Cette licence concerne ce que nous publions. Vous ne pouvez cependant pas vous faire
passer pour la coopérative Codeurs en Liberté.

Notre activité est soumise au respect de [notre charte d’éthique](/blog/ethique/charte/).
