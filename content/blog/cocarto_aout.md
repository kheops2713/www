+++
date = "2023-08-09T17:00:00+02:00"
title = "cocarto : billet d’étape n°9 (aout 2023)"
author = "Nicolas et Tristram"
categories = "cocarto"
+++

[cocarto](https://cocarto.com/) est un outil de saisie de données cartographiques et territoriales. Vous lisez la neuvième édition de la [lettre mensuelle](https://buttondown.email/cocarto) qui en raconte les développements, doutes, détails inutiles…

![Des campings municipaux dans le Finistère](campings_detail.jpg)

## Import, export, encore

On continue le chantier évoqué dans la précédente newsletter, sur l’échange des données vers et depuis cocarto. C’est peut-être *le* chantier majeur de cocarto. Qu’avons nous fait ces derniers mois ?

On peut désormais importer des données, pour une couche d’une carte, à partir de fichiers geojson, csv ou Excel et openoffice, mais aussi depuis un serveur [WFS](https://fr.wikipedia.org/wiki/Web_Feature_Service). En fait, c’est assez symbolique de notre vision de cocarto : être à l’interface entre les géomaticiens professionels et les utilisateurs-producteurs des données.

La prochaine étape consiste à configurer les imports : permettre de faire correspondre les colonnes aux attributs, gérer le format précis de représentation géographique (par exemple, du [WKT](https://fr.wikipedia.org/wiki/Well-known_text) ou deux colonnes “lat” et “long”). Là aussi, notre but est de proposer des fonctionnalités pour les débutants et les experts:
1. Déposer un fichier excel et automatiquement créer une carte, à partir de rien d’autre, de façon automatique.
2. Programmer des opérations quotidiennes de réimport de données distantes, avec une correspondance stricte entre les types champs, et envoyer un rapport d’erreur par email.

## En vrac

Nous avons également travaillé sur plein d’autres sujets ; voici un extrait de ce que vous pouvez désormais faire :

- avoir la carte ou le tableau en plein écran,
- changer la couleur sur la carte d’une couche de donnée,
- éditer une ligne dans un grand formulaire.

![](formulaire.png)

Et pour finir, une partie des améliorations :
- refonte complète de la gestion des permissions,
- moins d’en-têtes inutiles pour se concentrer sur la carte et le tableau de données,
- les pixels restent là où s’y attendrait lorsqu’on scrolle horizontalement un tableau avec de nombreuses colonnes.

![](permissions.png)

## Campings Municipaux

C’était une discussion de cet été: comment partir à vélo en tour de france avec un budget minimal ? C’est à ça que servent les campings municipaux (et aussi à éviter la soirée spéciale Michael Jackson).

Sur [OpenStreetMap](https://www.openstreetmap.org), c’est les `[tourism=camp_site]["operator:type"=public]`. Sauf que tous ne sont pas identifiés comme`public` : on a donc besoin aussi de `[tourism=camp_site][name~municipal]` pour espérer en obtenir la majeur partie. [Voici ce que ça donne réimporté dans cocarto.](https://cocarto.com/fr/share/rz5yHTSxG6fzDe1z)[^1] C’est un lien de partage de contribution : chacun peut ajouter des points, mais ne peut pas modifier ceux des autres.

[![Une carte des campings municipaux en France](campings.jpg)](https://cocarto.com/fr/share/rz5yHTSxG6fzDe1z)

C’est tout pour ce mois-ci ! En septembre prochain, nous serons aux [Geodatadays](https://www.geodatadays.fr) pour parler de cocarto à qui voudra bien nous entendre. Si vous êtes de passage, venez-donc nous voir. Au mois prochain pour la suite de nos aventures, et n’hésitez pas à nous envoyer vos remarques à bonjour@cocarto.com.

🗺️

[^1]: Attention, c’est pour la démo! Les données viennent d’OpenStreetMap, et il n’y a pas de réimport depuis cocarto vers OpenStreetMap. Rajoutons ça à la longue liste des fonctionnalités futures de cocarto 🕺.
