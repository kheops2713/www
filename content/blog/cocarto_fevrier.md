+++
date = "2023-03-06T19:31:00+01:00"
title = "cocarto : billet d’étape n°7 (février 2023)"
author = "Tristram"
categories = "cocarto"
+++

[cocarto](https://cocarto.com) est un outil de saisie de données cartographiques et territoriales. Vous lisez la septième édition de la lettre mensuelle qui en raconte les développements, doutes, détails inutiles…

![Schéma du marégraphe de Marseille](/img/maregraphe_marseille.jpeg)

# Des pixels et des pièces jointes

Février nous a occupé sur trois grands sujets. Les pièces jointes, ré-organiser le CSS et refaire la page d’accueil.

## Nouvelle page d’accueil

C’est un exercice toujours difficile : comment transmettre les services rendus par cocarto, vanter les avantages de ne pas être une startup risquant d’être rachetée et inviter les gens à nous contacter.

Nous avons donc passé du temps à refaire la  [page d’accueil](https://cocarto.com/presentation), choisir les illustrations et ré-écrire les textes. Espérons que ce soit plus clair maintenant !

## Des pièces jointes

Tout comme on peut associer du texte, des nombres ou encore des booléens à chaque objet géographique, il est désormais de définir une colonne de type _pièce jointe_.

Sur téléphone, vous pouvez même directement prendre une photo depuis l’appareil photo.

Cette fonctionnalité a été demandée par différents utilisateurs potentiels pour faciliter la collecte collaborative (répertorier une machine à laver abandonnée dans la forêt, faire remonter un site prometteur pour une installation photovoltaïque…).

## Ré-organiser les feuilles de style

Les [feuilles de style](https://fr.wikipedia.org/wiki/Feuilles_de_style_en_cascade) (ou CSS) sont un moyen de définir l’apparence (la couleur, la taille du texte, combien de marges…) du site. Elles nous ont donné du fil à retordre. Petit retour en arrière, plutôt destiné aux personnes ayant déjà eu à écrire du CSS.

Nous utilisons [Ruby on Rails](https://rubyonrails.org) pour tous nos développements.
Le fait que tous les besoins récurrents (tel que gérer des pièces jointes) soient intégrés, avec une manière canonique de traiter le problème nous permet de nous concentrer sur ce qui est réellement spécifique à notre produit.

Cependant, lorsqu’il s’agit des feuilles de style il n’y a pas vraiment d’approche préconisée.
Les outils existants ont des approches radicalement opposées et rails ne met aucune approche en avant.

Nous avons ainsi utilisé [Bulma](https://bulma.io/), [PicoCSS](https://picocss.com/) pour finalement écrire entièrement le CSS par nous-même. D’abord en _vanilla_ (sans aucun outillage), puis en SCSS avec [Dart](https://sass-lang.com/dart-sass).

Mais au-delà de l’outil, nous avons considérablement remanié l’organisation du code. Faut-il faire des classes très génériques à composer telles que `button`, `vert` , ou très proches de la fonction dans la page comme `confirmer-suppression-colonne` ?

Après beaucoup d’hésitations, et une ré-écriture assez longue, nous utilisons finalement le modèle [BEM](https://getbem.com/).

Nous avons donc pas mal erré, frustrés qu’il n’y a ait pas d’approche standard, mais le plus difficile est assurément derrière nous.

# Faire grève dans notre milieu ?

La réforme des retraites avec le recul de l’âge minimum et l’allongement de la durée de cotisation est le sujet d’actualité le plus vivace en France.

Tout comme [72 % des actifs](https://elabe.fr/reforme-retraites-2023-5/), nous soutenons le mouvement de contestation. Cependant, notre métier n’est pas essentiel. Si nous nous arrêtons de travailler, c’est à peine si quelqu’un s’en rendra compte. Comment donc faire remonter notre colère ?

Nous utilisons donc cette petite tribune pour contribuer à faire avancer cette réflexion et inviter plus de monde, selon leurs opinions et leurs capacités à faire grève, à manifester, à se syndiquer ou à débattre.

Comme point de départ, mentionnons par exemple l’article du [syndicat des travailleurs et travailleurs du jeu vidéo](https://www.stjv.fr/2023/02/9-idees-recues-et-questions-sur-la-greve-et-le-mouvement-contre-la-reforme-des-retraites/), ainsi que le site [La grève c’est cool](https://greve.cool/).

# Le caillou de Genève

Pour faire des cartes, connaitre la hauteur est très utile ! Et pour cela il faut un point de référence. Ainsi le niveau de la mer est défini en France par le [marégraphe de Marseille](https://fr.wikipedia.org/wiki/Marégraphe_de_Marseille).

La Suisse, n’ayant pas de mer à utiliser comme référence, a choisi au 19ème siècle la [Pierre du Niton](https://cms.geo.admin.ch/www.swisstopo.admin.ch/archives/cms2007/internet/swisstopo/fr/home/topics/survey/faq/niton.html) comme référence. Elle se situe à Genève dans le lac Léman, 376,86 mètres au-dessus du niveau de la mer à Marseille.

En 1902, des mesures plus précises ont établi qu’en réalité cette référence n’était que 373,60 mètres au-dessus du niveau de la mer. C’est ainsi que la Suisse s’est enfoncée de 3,26 mètres du jour au lendemain.

# Conclusion

N’hésitez pas tester cocarto, nous dire ce que vous en pensez, ce qui vous manque, ce qui vous agace et comment vous pourriez l’utiliser. Contactez-nous : bonjour@cocarto.com
