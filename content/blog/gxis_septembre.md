+++
date = "2022-10-04T14:01:00+01:00"
title = "Ĝis : billet d’étape n°4 septembre 2022"
author = "Tristram"
categories = "cocarto"
+++

[Ĝis](https://gxis.codeursenliberte.fr) est un outil de saisie de données cartographiques et territoriales. Vous lisez la quatrième édition de la lettre mensuelle qui en raconte les développements, doutes, détails inutiles…

 ![Carte de Mercator de 1569](/img/Mercator_1569.png)

# Que s’est-il passé en aout et en septembre ?

En aout, pas grand chose, nous avons profité de l’été.

Grâce au travail de Chris, stagiaire chez Codeurs en Liberté, vous pouvez désormais zoomer sur l’ensemble des éléments, ou sur un élément particulier.

Après la gestion des droits en juillet, vous pouvez desormais créer un lien de partage anonyme. Plus besoin de créer un compte pour inviter des personnes à travailler avec vous. Cela sera, nous l’espérons, d’une grande aide pour des opérations de crowdsourcing.

Nous avons également poussé les tests de performance et trouvé quelques ralentissement ; au niveau du rendu côté serveur, mais surtout — ce qui nous a surpris — côté navigateur, pour charger la page. Mais désormais il est parfaitement possible d’être plusieurs à travailler simultanément sur une carte avec un millier de tracés.

Enfin, nous avons rencontré deux associations qui souhaitent utiliser ĝis pour leurs besoins. Cela changera notre rapport au produit : désormais il faudra être deux fois plus attentif qu’un changement n’a pas d’effet indésirable et nous avons des retours très concrets qui vont beaucoup plus influencer nos développements.

# Cas d’usage : observatoire du plan vélo
Un [exemple combinant tous les éléments précédent](https://gxis.codeursenliberte.fr/fr/share/c9hVfjqAM97d5CsQ) : l’association [Paris en Selle](https://parisenselle.fr/) maintient l’observatoire du plan vélo de la ville de Paris.

Une douzaine de bénévoles ont travaillé ensemble une après-midi pour repertorier tous les axes et qualifier leur avancement et qualité de réalisation. Les données sont disponibles en [geojson](https://geojson.org/) pour être intégrée directement dans le site du plan vélo. Au fur et à mesure du déploiement du plan, un simple changement dans la colonne « Statut » permettra de constater l’évolution et de l’afficher en conséquence sur le site.


# Cartographe du mois : Mercator

La projection de Mercator est souvent raillée. Elle est à la cartographie ce que le Comic Sans MS est à la typographie, la Fiat Multipla à la voiture ou la pizza à l’ananas à la gastronomie : une moquerie récurrente parfois injuste.

Pour le [courrier de juillet](https://www.codeursenliberte.fr/blog/gxis_juillet/) nous avions parlé des [portulans](https://fr.wikipedia.org/wiki/Portulan) qui permettent de savoir quel cap suivre pour arriver à destination, sans être généralisable à la terre entière.

Pour répondre à ce problème, Mercator publie en 1569 la carte qui deviendra célèbre et la base de la projection omni-présente et _de facto_ notre représentation du monde.

Afin de pouvoir servir à la navigation à la boussole, les distances et les aires sont sacrifiées (entrainant notre vision faussée du monde). La formule de projection nécessite l’utilisation du logarithme, qui n’avait pas encore été découvert à son époque. C’est donc une approche très empirique et itérative qui a permis de construire la carte. Impressionant !

Mais aujourd’hui, où plus personne ne traverse les océans avec uniquement une boussole, cette projection n’a plus aucune utilité. Mais reconnaissons son utilité historique et le génie pour créer cette carte qui nécessite des concepts mathématiques qui n’étaient même connu.

Ironiquement, un certain [Nicolaus Mercator](https://fr.wikipedia.org/wiki/Nicolaus_Mercator) (_a priori_ aucun lien familial) a pas mal étudié les logarithmes, un siècle plus tard.

Hélas, pour l’instant, [Maplibre](https://maplibre.org/) que nous utilisons pour afficher la carte utilise la projection [Web Mercator](https://en.wikipedia.org/wiki/Web_Mercator_projection).

# Conclusion

Vous voulez faire parti des beta-testeurs et beta-testeuses qui donneront le cap au produit ? Contactez-nous : bonjour@codeursenliberte.fr
