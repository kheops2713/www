+++
date = "2016-09-03T20:01:41+02:00"
title = "Philosophie"
draft = true
no_date = true
+++
## Engagement

Notre raison d’être est militante : nous cherchons à explorer notre relation au
travail.

Chaque membre de _Codeurs en Liberté_ attache une grande importance au travail
fait avec amour. Ainsi, chacun peut travailler moins pour travailler mieux
pour garantir la qualité de ses contributions et un équilibre avec la vie
privée.

De plus, nous exigeons de chaque membre qu’il s’approprie l’entreprise. Cela
passe par un actionnariat obligatoire et — en reprenant les principes des
coopératives — la possession d’une voix quelque soit le montant de capital
détenu.

## Transparence

Nous sommes convaincus que la transparence doit être le comportement par défaut
et qu’elle est nécessaire pour permettre l’implication réelle de chaque membre.
Nous poussons cette transparence en rendant public la majeure partie de notre
activité.

Seules les négociations commerciales et les données à caractère personnel ne
sont pas publiées.

Par exemple, les décisions de nos assemblées générales, ou encore les échanges
sur divers points de fonctionnement (et pas que le résultat final) peuvent être
consultés sur <https://gitlab.com/CodeursEnLiberte/fondations/>.

## Transmission de connaissances

Beaucoup d’énergie est perdue en informatique en redéveloppant les mêmes choses.

Nous essayons de faire un grand effort de documentation de nos réalisations et
de permettre leur ré-utilisation.

Par ailleurs, malgré des membres et des clients aux langues maternelles variées,
nous nous efforçons de ne pas retomber sur l’anglais comme choix par défaut.

## Le grand écart salarié—indépendant

Choisir le temps de travail et les missions nous semble indispensable.
Cependant cela est à première vue incompatible avec un statut de salarié.

Cependant, la protection sociale des indépendants est selon nous trop
faible par rapport à un salarié affilié au régime général.

Évidemment, nous avons des métiers beaucoup plus faciles, loin de toute précarité :
bien rémunérés et le travail ne manque pas. Cependant par cette structure, nous
souhaitons contribuer à faire émerger de nouvelles formes de relation au travail.
