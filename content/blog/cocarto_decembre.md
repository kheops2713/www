+++
date = "2022-12-23T10:28:00+01:00"
title = "cocarto : billet d’étape n°5 décembre 2022"
author = "Tristram"
categories = "cocarto"
+++

[cocarto](https://cocarto.com) est un outil de saisie de données cartographiques et territoriales. Vous lisez la cinquième édition de la lettre mensuelle qui en raconte les développements, doutes, détails inutiles…

![Logotype de cocarto](/img/cocato_cartouche_couleur.png)

# Bilan de l’année.

Avant tout : nous avons un nom : _cocarto_ ! Fini les accents circonflexes sur le `ĝ` que seuls les espérantistes savent prononcer. Nous mettons ainsi en avant l’aspect collaboratif de notre outil.

Cette grande étape ayant (enfin !) été franchie, et parce que c’est la fin d’année, regardons un peu où nous en sommes.

Nous nous sommes lancés sur ce projet suite à notre [assemblée générale du 18 mars](/blog/ag_2021).

Entre l’été, le temps partiel, le covid, les missions que nous avons continué pour gagner de l’argent, nous avons consacré environ 10 mois-personne au développement.

Bien évidemment par perfectionnisme, nous souhaitons aller plus loin sur l’interface, les fonctionnalités et la base de code.
Mais si nous attendons la perfection, jamais _cocarto_ ne rendra service.

Nous pensons que _cocarto_ peut déjà être mis dans des mains et qu’il rendra service.
Ce sont des vrais retours sur les blocages et les petits agacements qui permettront d’améliorer le produit et d’infléchir la feuille de route.

# Gagner de l’argent

50 000 € ont été investis en fonds propres par Codeurs en Liberté. Cette somme est quasiment épuisée.

Pour l’instant nous n’avons encore touché aucune somme extérieure.
Il faudra environ 100 000 €/an pour continuer à faire vivre le produit et pour qu’il réponde à de plus en plus de cas d’usages.

Nous ne comptons pas lever d’argent pour ne pas avoir de pression poussant à l’hyper-croissance.

Comme nous l’avions évoqué dans l’[infolettre de juillet](/blog/gxis_juillet), nous espérons financer la suite par l’accès au logiciel en SaaS, des fonctionnalités spécifiques et l’accompagnement personnalisé.

2023 sera donc l’année où nous devrons faire ce que nous n’avons pas envie de faire : du commercial ! Peut-être que vous recevrez un email — personnalisé car nous ne sommes pas des sauvages — afin de savoir comment _cocarto_ pourra vous aider dans la saisie de vos données cartographiques.

Et si vous voulez nous aider, parlez de _cocarto_ autour de vous.

# Question éthique

Au sein de Codeurs en Liberté nous avons adopté une [charte d’éthique](/blog/ethique/) qui nous pousse à nous poser des questions à la fois sur les clients que l’on choisit, mais aussi sur les technologies que l’on utilise, et par qui elles sont développées.

Cela nous a amené à questionner les choix technologiques spécifiques faits pour _cocarto_.

En effet, nous nous basons sur le framework web [Ruby on Rails](https://rubyonrails.org/) qui structure toute l’application.

Ce framework a été initialement développé par [David Heinemeier Hansson](https://en.wikipedia.org/wiki/David_Heinemeier_Hansson), souvent abbrégé _DHH_.
Même si la communauté s’est fortement développé, il reste une personnalité très influente et très visible.

Il a tenu des propos réactionnaires répétés et sa présence dans la communauté Ruby on Rails [est un problème](https://tomstu.art/the-dhh-problem). Un blog post en novembre 2022 a déclenché de très vives discussions dans la communauté travaillant avec Ruby on Rails jusqu’à poser la question d’arrêter d’utiliser ce framework auquel nous tenons.

Une [fondation rails](https://rubyonrails.org/2022/11/14/the-rails-foundation) avait été annoncée une semaine plus tôt.
Cela aurait pu être une excellente sortie par le haut pour aller vers un représentation plus collégiale.
Malheureusement, DHH en est à la tête, laissant le problème intacte.

Pour l’instant, nous restons sur Ruby on Rails. Mais nous restons vigilants et peut-être qu’il faudra un jour migrer vers une technologie qui n’ait pas une personnalité unique trop visible.

# Conclusion

Avec la fin d’année, se finit la phase de création de la preuve de concept. Nous sommes très contents.

Vous voulez faire partie des bêta-testeurs et bêta-testeuses qui donneront le cap au produit ? Contactez-nous : bonjour@cocarto.com
